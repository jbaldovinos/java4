package deloitte.academy.lesson4.entity;

import deloitte.academy.lesson4.enums.TipoHuesped;

public abstract class CheckIn {

	private String nombre;
	private TipoHuesped tipoDeHuesped;
	private int asignacionDeHabitacion;
	private int dias;

	public CheckIn() {
		// TODO Auto-generated constructor stub
	}

	public CheckIn(String nombre, TipoHuesped tipoDeHuesped, int asignacionDeHabitacion, int dias) {
		super();
		this.nombre = nombre;
		this.tipoDeHuesped = tipoDeHuesped;
		this.asignacionDeHabitacion = asignacionDeHabitacion;
		this.dias = dias;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoHuesped getTipoDeHuesped() {
		return tipoDeHuesped;
	}

	public void setTipoDeHuesped(TipoHuesped tipoDeHuesped) {
		this.tipoDeHuesped = tipoDeHuesped;
	}

	public int getAsignacionDeHabitacion() {
		return asignacionDeHabitacion;
	}

	public void setAsignacionDeHabitacion(int asignacionDeHabitacion) {
		this.asignacionDeHabitacion = asignacionDeHabitacion;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	public abstract double cobrar();

}
