package deloitte.academy.lesson4.entity;

/**
 * 
 * @author jbaldovinos
 *
 */
public abstract class Hotel {

	private String nombre;
	private int tipoDeHuesped;
	private int asignacionHabitacion;

	/**
	 * tipoDeHuesped = 1 = Frecuente tipoDeHuesped = 2 = Empleado
	 */

	public Hotel() {
		nombre = "Julio";
		tipoDeHuesped = 1;
		asignacionHabitacion = 1;

	}

	public Hotel(String nombre, int tipoDeHuesped, int asignacionHabitacion) {
		super();
		this.nombre = nombre;
		this.tipoDeHuesped = tipoDeHuesped;
		this.asignacionHabitacion = asignacionHabitacion;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTipoDeHuesped() {
		return tipoDeHuesped;
	}

	public void setTipoDeHuesped(int tipoDeHuesped) {
		this.tipoDeHuesped = tipoDeHuesped;
	}

	public int getAsignacionHabitacion() {
		return asignacionHabitacion;
	}

	public void setAsignacionHabitacion(int asignacionHabitacion) {
		this.asignacionHabitacion = asignacionHabitacion;
	}

}
