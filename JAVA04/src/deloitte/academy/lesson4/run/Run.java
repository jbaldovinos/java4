package deloitte.academy.lesson4.run;

import java.util.ArrayList;

import deloitte.academy.lesson4.entity.CheckIn;
import deloitte.academy.lesson4.logic.Empleado;
import deloitte.academy.lesson4.logic.Huesped;
import deloitte.academy.lesson4.logic.HuespedFrecuente;
import deloitte.academy.lesson4.enums.TipoHuesped;

public class Run {

	public static ArrayList<Huesped> listaHuesped = new ArrayList<Huesped>();
	public static ArrayList<HuespedFrecuente> listaHuespedFrecuente = new ArrayList<HuespedFrecuente>();
	public static ArrayList<Empleado> listaEmpleados = new ArrayList<Empleado>();

	public static void main(String[] args) {

		/**
		 * Se genera la lista de huespedes de tipo empleado
		 */
		Huesped registroHuesped = new Huesped("Daniela Reyes", TipoHuesped.Huesped, 1, 3);
		HuespedFrecuente registroHuesped1 = new HuespedFrecuente("Daniela Navarrete", TipoHuesped.Huesped, 2, 3);
		Empleado registroHuesped2 = new Empleado ("Emiliano Olivers",TipoHuesped.Huesped, 3, 3);
		


		listaHuesped.add(registroHuesped);
		listaHuespedFrecuente.add(registroHuesped1);
		listaEmpleados.add(registroHuesped2);
		
		System.out.println("Lista de Huesped: " + listaHuesped.get(0).getNombre());
		System.out.println("Lista de Huesped Frecuente: " + listaHuespedFrecuente.get(0).getNombre());
		System.out.println("Lista de Empleados:  " + listaEmpleados.get(0).getNombre());
		
		Cobrar(registroHuesped);
		Cobrar(registroHuesped1);
		Cobrar(registroHuesped2);
		
		
	}

	public static void Cobrar(CheckIn checkIn) {
		System.out.println(checkIn.cobrar());
	}
	

}