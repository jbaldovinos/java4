package deloitte.academy.lesson4.logic;

import deloitte.academy.lesson4.entity.CheckIn;
import deloitte.academy.lesson4.enums.TipoHuesped;

public class Empleado extends CheckIn {

	/**
	 * Clase encargada de generar la cuota a pagar por los d�as que se hospedara
	 * 
	 */
	public Empleado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Empleado(String nombre, TipoHuesped tipoDeHuesped, int asignacionDeHabitacion, int dias) {
		super(nombre, tipoDeHuesped, asignacionDeHabitacion, dias);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		return (this.getDias() * (100 * .80));
	}
}
