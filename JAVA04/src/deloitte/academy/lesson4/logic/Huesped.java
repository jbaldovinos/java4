package deloitte.academy.lesson4.logic;

import deloitte.academy.lesson4.entity.CheckIn;
import deloitte.academy.lesson4.enums.TipoHuesped;

/**
 * 
 * @author jbaldovinos Clase huesped heredado al CheckIn
 */
public class Huesped extends CheckIn {

	public Huesped() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Huesped(String nombre, TipoHuesped tipoDeHuesped, int asignacionDeHabitacion, int dias) {
		super(nombre, tipoDeHuesped, asignacionDeHabitacion, dias);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		return (this.getDias() * (100));
		// return 100*1*(this.getDias());
	}

}
