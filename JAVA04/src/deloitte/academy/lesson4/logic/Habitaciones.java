package deloitte.academy.lesson4.logic;

/**
 * 
 * @author jbaldovinos 
 * Clase encargada de generar la lista de habitaciones
 */
public class Habitaciones {
	public void HabitacionesDisponible() {
		String[] habitaciones = new String[10];
		habitaciones[0] = ("Ocupada");
		habitaciones[1] = ("Ocupada");
		habitaciones[2] = ("Disponible");
		habitaciones[3] = ("Ocupada");
		habitaciones[4] = ("Disponible");
		habitaciones[5] = ("Disponible");
		habitaciones[6] = ("Ocupada");
		habitaciones[7] = ("Disponible");
		habitaciones[8] = ("Ocupada");
		habitaciones[9] = ("Ocupada");

	}

}
