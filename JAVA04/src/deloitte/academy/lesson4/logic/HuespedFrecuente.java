package deloitte.academy.lesson4.logic;

import deloitte.academy.lesson4.entity.CheckIn;
import deloitte.academy.lesson4.enums.TipoHuesped;

public class HuespedFrecuente extends CheckIn {

	/**
	 * Clase que genera el Huesped Frecuente junto con cuota a pagar por el
	 * hospedaje
	 */
	public HuespedFrecuente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HuespedFrecuente(String nombre, TipoHuesped tipoDeHuesped, int asignacionDeHabitacion, int dias) {
		super(nombre, tipoDeHuesped, asignacionDeHabitacion, dias);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub

		return (this.getDias() * (100 * .90));
	}

}
